package StepDefination;

import PageObject.AddCustomerPage;
import PageObject.LoginPage;
import PageObject.SearchCustomerPage;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebDriver;

public class BaseClass {


    public static WebDriver driver;
    public LoginPage loginPage;
    public AddCustomerPage addCustomerPage;
    public SearchCustomerPage searchCustomerPage;


    public String genarateEmailID(){
        return( RandomStringUtils.randomAlphabetic(5));
    }
}
