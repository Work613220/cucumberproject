package StepDefination;

import PageObject.AddCustomerPage;
import PageObject.LoginPage;
import PageObject.SearchCustomerPage;
import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class StepDef extends BaseClass{

//    public WebDriver driver;
//    public LoginPage loginPage;
//    public AddCustomerPage addCustomerPage;
//    public SearchCustomerPage searchCustomerPage;

    @Given("^User Launch Chrome browser$")
    public void user_launch_chrome_browser() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        loginPage = new LoginPage(driver);
        addCustomerPage = new AddCustomerPage(driver);
        searchCustomerPage = new SearchCustomerPage(driver);
    }

    @When("User opens URL {string}")
    public void user_opens_url(String url) {
        driver.get(url);
        driver.manage().window().maximize();

    }

    @When("User enters Email as {string} and Password as {string}")
    public void user_enters_email_as_and_password_as(String emailAdd, String password) {
        loginPage.enterPassword(emailAdd);
        loginPage.enterPassword(password);

    }

    @When("Click on Login")
    public void click_on_login() {
        loginPage.clickOnLoginButton();

    }

   // @Then("Page Title should be {string}")
     @Then("^Page Title should be \"([^\"]*)\"$")
    public void page_title_should_be(String expectedTitle) {
        String actualTitle = driver.getTitle();
        if (expectedTitle.equals(actualTitle)) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false);
        }
    }

    @When("User click on Log out link")
    public void user_click_on_log_out_link() {
        loginPage.clickOnLogOutButton();

    }

    @Then("close browser")
    public void close_browser() {
        driver.close();
        driver.quit();

    }

    ////////////// Add New Customer
    @Then("User can view Dashboad")
    public void user_can_view_dashboad() {
        String actualTitle = addCustomerPage.getPageTitle();
        String expectedTitle = "Dashboard / nopCommerce administration";
        if (actualTitle.equals(expectedTitle)) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false);

        }
    }

    @When("User click on customers Menu")
    public void user_click_on_customers_menu() throws InterruptedException {
        addCustomerPage.clickOnCustomersMenu();
    }

    @When("click on customers Menu Item")
    public void click_on_customers_menu_item() throws InterruptedException {
        addCustomerPage.clickOnCustomersMenuItem();
    }

    @When("click on Add new button")
    public void click_on_add_new_button() {
        addCustomerPage.clickOnAddnew();
    }

    @Then("User can view Add new customer page")
    public void user_can_view_add_new_customer_page() {
        String actualTitle = addCustomerPage.getPageTitle();
        String expectedTitle = "Add a new customer / nopCommerce administration";
        if (actualTitle.equals(expectedTitle)) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false);
        }

    }

    @When("User enter customer info")
    public void user_enter_customer_info() throws InterruptedException {

        addCustomerPage.enterEmail(genarateEmailID()+"@gmail.com");
        addCustomerPage.enterPassword("test1");
        addCustomerPage.enterFirstName("jeevika");
        addCustomerPage.enterLastName("maidarkar");
        addCustomerPage.enterGender("Female");
        addCustomerPage.enterDob("11/15/2014");
        addCustomerPage.enterCompanyName("coforge");
        addCustomerPage.enterManagerOfVendor("Vendor 1");
        addCustomerPage.enterAdminContent("admin");
        Thread.sleep(2000);
    }

    @When("click on Save button")
    public void click_on_save_button() {
        addCustomerPage.clickOnSave();
    }

    @Then("User can view confirmation message {string}")
    public void user_can_view_confirmation_message(String expectedMessage) throws InterruptedException {
        Thread.sleep(2000);
        String bodyTagText = driver.findElement(By.tagName("Body")).getText();
        if (bodyTagText.contains(expectedMessage)) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false);
        }

    }
    @When("Enter customer EMail")
    public void enter_customer_e_mail() {
        searchCustomerPage.enterEmailAdd("admin@yourStore.com");
    }
    @When("Click on search button")
    public void click_on_search_button() throws InterruptedException {
        searchCustomerPage.clickOnSearchButton();
        Thread.sleep(3000);
    }
    @Then("User should found Email in the Search table")
    public void user_should_found_email_in_the_search_table() {
        String expectedEmail = "admin@yourStore.com";
        Assert.assertTrue((searchCustomerPage.searchCustomerByEmail(expectedEmail)));
    }

    //  search By Name

    @When("Enter customer FirstName")
    public void enter_customer_first_name() {
        searchCustomerPage.enterFirstName("John");
    }
    @When("Enter customer LastName")
    public void enter_customer_last_name() {
        searchCustomerPage.enterLastName("Smith");
    }
    @Then("User should found Name in the Search table")
    public void user_should_found_name_in_the_search_table() {
        String expectedName = "John Smith";
        if(searchCustomerPage.searchCustomerByName(expectedName)){
            Assert.assertTrue(true);
        }else {
            Assert.assertTrue(false);
        }
    }
}
