package StepDefHooks;

import PageObject.AddCustomerPage;
import PageObject.LoginPage;
import PageObject.SearchCustomerPage;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class stepdef {

    public WebDriver driver;
    public LoginPage loginPage;

  //  @Before(order = 0)
    @Before("@Sanity")
    //@Before
    public void setup(){
        System.out.println("setup method executed");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

   // @Before(order=1)
   @Before("@Regression")
   //@Before(order = 1)
    public void setup1(){
        System.out.println("setup1 with Order 1 method executed");
       // WebDriverManager.chromedriver().setup();
      // driver = new ChromeDriver();
    }
@BeforeStep
    public void beforeStepMethodDemo(){
        System.out.println("Before each step method executed ");
    }


    @Given("User Launch Chrome browser")
    public void user_launch_chrome_browser() {
        loginPage = new LoginPage(driver);

    }

    @When("User opens URL {string}")
    public void user_opens_url(String url) {
        driver.get(url);
        driver.manage().window().maximize();

    }

    @When("User enters Email as {string} and Password as {string}")
    public void user_enters_email_as_and_password_as(String emailAdd, String password) {
        loginPage.enterPassword(emailAdd);
        loginPage.enterPassword(password);

    }

    @When("Click on Login")
    public void click_on_login() {
        loginPage.clickOnLoginButton();

    }

    @Then("Page Title should be {string}")
    public void page_title_should_be(String expectedTitle) {
        String actualTitle = driver.getTitle();
        if (expectedTitle.equals(actualTitle)) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false);
        }
    }

    @When("User click on Log out link")
    public void user_click_on_log_out_link() {
        loginPage.clickOnLogOutButton();

    }

    @Then("close browser")
    public void close_browser() {
        driver.close();
        //driver.quit();

    }

   // @After(order=0)
    @After("@Sanity")
    //@After
    public void tearDown(){
        System.out.println("Teardown method executed");
        driver.quit();
    }

  //  @After(order=1)
    @After("@Regression")
   // @After
    public void tearDown1(){
        System.out.println("Teardown1 method executed");
       //driver.quit();
    }
@AfterStep
    public void afterStepMethodDemo(){
        System.out.println("After each Step method executed");

    }
}
