package Runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

//import io.cucumber.junit.Cucumber;
//import io.cucumber.junit.CucumberOptions;
//import org.junit.runner.RunWith;
//@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/Features/",
        glue = "StepDefination",
        dryRun = false,
        monochrome = true,
        tags = "@regression",
        plugin = {"pretty","html:target/cucumber-reports/reports_html.html"}
      //          "json:target/cucumber-reports/reports_json.json",
//                "junit:target/cucumber-reports/reports_xml.xml"}
)
public class Run extends AbstractTestNGCucumberTests {
}

