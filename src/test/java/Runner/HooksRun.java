package Runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/FeaturesHooks/",
        glue = "StepDefHooks",
        dryRun = false,
        monochrome = true,
        //tags = "@Regression",
        plugin = {"pretty","html:target/cucumber-reports/reports_html.html"}
        //          "json:target/cucumber-reports/reports_json.json",
//                "junit:target/cucumber-reports/reports_xml.xml"}
)
public class HooksRun {
}
